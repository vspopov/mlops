## Repository Management Methodology

In our repository, we employ a structured approach to branch management:

* **experiment** branch: This branch serves as a sandbox for conducting experiments on data. It is dedicated to exploring datasets, identifying patterns, and conducting in-depth analyses. The experimentation branch allows us to test hypotheses and innovative approaches without impacting the stability of the main development branch.

* **model** branch: The model branch is reserved for building models on validated and clean datasets. It is utilized for refining and testing various modeling techniques, optimizing parameters, and assessing model performance. This branch ensures that only robust and well-tested models are integrated into the main development branch for further deployment or integration.

* **master** branch: The master branch represents the main line of development. It encompasses the initial project setup and the final results. The master branch reflects the stable and production-ready version of the project.