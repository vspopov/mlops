
The flake8 use in this project.

Installation pip install flake8

Usage: flake8 

Options:

- version: Show the version and exit
- help: Show this message and exit
- exclude: Comma-separated list of files or directories to exclude
- ignore: Comma-separated list of errors and warnings to ignore
- max-line-length: Maximum allowed line length (default: 79)
- max-complexity: Maximum allowed McCabe complexity score of a function (default: 10)
- statistics: Count errors and warnings
- count: Count total number of errors and warnings
- exit-zero: Exit with status code 0 even if there are errors
- show-source: Show source code for each error
- format: Set the error format (default: "default")